﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class CambridgeController : Controller
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        // GET: Cambridge


        public ActionResult Index()
        {

            return View();
        }
        public ActionResult WhyCambridgeEnglish()
        {
            return View();
        }
        public ActionResult ExamandTest()
        {
            return View();
        }
        public ActionResult YLEStarter()
        {
            return View();
        }
        public ActionResult YLEMovers()
        {
            return View();
        }
        public ActionResult YLEFlyers()
        {
            return View();
        }
        public ActionResult LearningEnglish()
        {
            return View();
        }
        public ActionResult ExamPreparation()
        {
            return View();
        }
        public ActionResult TestYourEnglish()
        {
            return View();
        }
        public ActionResult ParentsandChildren()
        {
            return View();
        }
        public ActionResult blog()
        {
            return View();
        }
        public ActionResult news()
        {
            return View();
        }
        public ActionResult Help()
        {
            return View();
        }
        public ActionResult Enquiriesandappeals()
        {
            return View();
        }
    }
}