﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class AboutController : Controller
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        // GET: About
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult odm_difference()
        {
            return View();
        }
        public ActionResult Chairman_Message()
        {
            return View();
        }
        public ActionResult University_Placements()
        {
            return View();
        }
        public ActionResult odm_educational_group()
        {
            return View();
        }
        public class awardsclass
        {
            public Nullable<int> year { get; set; }
            public List<Tbl_School_Awards> awards { get; set; }

        }
        public ActionResult awards_and_accolades()
        {
            var years = Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true).Select(a=>a.year).Distinct().ToList().OrderByDescending(a=>a.Value);
            if (years.Count() > 0)
            {
                List<awardsclass> ob = new List<awardsclass>();
                foreach (var y in years)
                {
                    awardsclass ab = new awardsclass();
                    ab.year = y;
                    ab.awards = Dbcontext.Tbl_School_Awards.Where(a => a.is_active == true && a.year==y).ToList();
                    ob.Add(ab);
                }
                ViewBag.awards = ob;
            }
            else
            {
                ViewBag.awards = null;
            }
            return View();
        }
    }
}