﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult pagenotfound()
        {
            return View();
        }
        public ActionResult pagedeleted()
        {
            return View();
        }
    }
}