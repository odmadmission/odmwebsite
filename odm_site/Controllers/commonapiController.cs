﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace odm_site.Controllers
{
    public class commonapiController : ApiController
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        public class contact_Us_class
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string purpose { get; set; }
            public string message { get; set; }
            public string feedback { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetContactus()
        {
            try
            {
                var contact = Dbcontext.tbl_school_contact_us.Where(a => a.is_active == true).ToList();
                var list = (from a in contact.ToList()
                           select new contact_Us_class
                           {
                               purpose = a.purpose,
                               email = a.email,
                               feedback = a.feedback,
                               id = a.id,
                               inserted_on = a.inserted_on,
                               message = a.message,
                               name = a.name,
                               phone = a.phone
                           }).ToList().OrderByDescending(a => a.inserted_on);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception)
            {              
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }
        [HttpDelete]
        public HttpResponseMessage RemoveContactus(int id)
        {
            try
            {
                var ob = Dbcontext.tbl_school_contact_us.Where(a => a.id == id).ToList();
                if (ob.Count > 0)
                {
                    tbl_school_contact_us obj = Dbcontext.tbl_school_contact_us.Where(a => a.id == id).FirstOrDefault();
                    obj.is_active = false;
                    Dbcontext.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, 1);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }
        public class Admission_Enquiry_class
        {
            public int id { get; set; }
            public string firstname { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string lastname { get; set; }
            public string presentschool { get; set; }
            public string className { get; set; }
            public string description { get; set; }
            public string city { get; set; }
            public string feedback { get; set; }
            public Nullable<System.DateTime> dob { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetAdmissionEnquiry()
        {
            try
            {
                var admission = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.is_active == true).ToList();
                var classes = Dbcontext.Tbl_School_Class.ToList();
                var res = (from a in admission
                           join c in classes.ToList() on a.classid equals c.Class_Id
                           select new Admission_Enquiry_class
                           {
                               firstname = a.firstname,
                               lastname = a.lastname,
                               city = a.city,
                               id = a.id,
                               inserted_on = a.inserted_on,
                               description = a.description,
                               email = a.email,
                               phone = a.phone,
                               presentschool = a.presentschool,
                               dob = a.dob,
                               className = c.Class_Name,
                               feedback = a.feedback
                           }).ToList().OrderByDescending(a => a.inserted_on);
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }
        [HttpDelete]
        public HttpResponseMessage RemoveAdmissionEnquiry(int id)
        {
            try
            {
                var ob = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.id == id).ToList();
                if (ob.Count > 0)
                {
                    Tbl_Admission_Enquiry obj = Dbcontext.Tbl_Admission_Enquiry.Where(a => a.id == id).FirstOrDefault();
                    obj.is_active = false;
                    Dbcontext.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, 1);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }
    }
}
