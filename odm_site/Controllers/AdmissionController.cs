﻿using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class AdmissionController : Controller
    {
        DB_ODMSchoolEntities Dbcontext = new DB_ODMSchoolEntities();
        // GET: Admission
        public ActionResult admission_details()
        {
            ViewBag.classlist = Dbcontext.Tbl_School_Class.Where(a => a.Is_Active == true).ToList();
            return View();
        }
        public PartialViewResult _Admissionenquiry()
        {
            return PartialView();
        }
        public ActionResult plan_a_visit()
        {
            ViewBag.classlist = Dbcontext.Tbl_School_Class.Where(a => a.Is_Active == true).ToList();
            return View();
        }
        public ActionResult OSAT_2020_and_N40()
        {
            return View();
        }
        public ActionResult Scholarships()
        {
            return View();
        }
    }
}