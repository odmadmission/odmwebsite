//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_maintenancestaff
    {
        public int maintenancestaaffId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public Nullable<int> staffcategoryId { get; set; }
        public string mobile { get; set; }
        public string image { get; set; }
        public string idproofcategory { get; set; }
        public string idproofnumber { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<System.DateTime> inserted_on { get; set; }
    }
}
