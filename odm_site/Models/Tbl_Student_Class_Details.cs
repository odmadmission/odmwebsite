//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Student_Class_Details
    {
        public int Student_Class_Id { get; set; }
        public Nullable<int> Student_Id { get; set; }
        public Nullable<int> Class_Id { get; set; }
        public Nullable<int> Session_Id { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public string Status { get; set; }
        public string Section { get; set; }
        public Nullable<System.DateTime> Modified_on { get; set; }
    }
}
