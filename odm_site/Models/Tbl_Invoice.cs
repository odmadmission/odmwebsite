//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Invoice
    {
        public long ID { get; set; }
        public string EmployeeName { get; set; }
        public string Department { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }
        public string Purpose { get; set; }
        public Nullable<decimal> Amount_1 { get; set; }
        public Nullable<decimal> Total_Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> Invoice_Code { get; set; }
    }
}
